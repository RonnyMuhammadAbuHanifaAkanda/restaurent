@extends('layouts.master')

@section('content')
<div class="row">
      <div class="col-lg-12">
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i><a href="{{URL::to('/dashboard')}}">Home</a></li>
          <li><i class="fa fa-laptop"></i>Dashboard</li>
        </ol>
      </div>
    </div>

@include('layouts.sidebar.mainBody')

@endsection
